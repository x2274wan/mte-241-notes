# Terminology/Nomenclature
- **Fault**: abnormal condition or defect
	- e.g. lighting strike, design flaw, metal fatigue
- **Error**: deviation of actual state from correct (expected?) state; essentially specific instances of faults
	- e.g. voltage overload, undulating suspension bridge, broken fan blade
- **Failure**: deviation from expected behaviour; the consequence of errors
	- e.g. Apollo 12 systems offline, bridge collapse, engine explosion

## Methods to Deal with Faults
- **Avoidance**: prevent faults
	- e.g. improve launch protocols, wind tunnel prototype testing, x-ray inspection of fan blades
- **Tolerance**: prevent failures
	- continue to operate within spec
		- e.g. redundant components
	- graceful degradation of service
		- e.g. thermal throttling of a processor
	- fail safe - halt in a safe manner
		- e.g. deadman switch (stop operation as soon as some condition isn't satisfied, like watchdog timers)

# Clock Synchronization
- distributed components have their own clocks
- clock pulses can be generated from quartz crystall oscillators
	- it can be affected by temperature and supply voltage
	- it drifts by about 0.6s / week
- options to maintain common time:
	- **centralized**: standard clock, denoted as $C_s$, where other clocks synchronize to it
	- **distributed**: synchronize between peers themselves

## Properties
- clock $C_i$ gives time $C_i(t)$ at *real* (ground truth) time $t$
	1. thus **correctness** can be described as $|C_i(t) - t| < \epsilon$
		- produces the correct "offset"
	2. bounded **drift rate** shall be $\left|\frac{dC_i(t)}{dt} - 1\right| < \rho$
		- samples at the rate of *real* time
	3. **monotonicity** is $C_i(t_{k+1}) \ge C_i(t_{k})$ where $t_{k+1} > t_{k}$
		- always goes forward
	4. **chronoscopicity**: if $t_2 - t_1 = t_4 - t_3$, then $C_i(t_2) - C_i(t_1) \approx C_i(t_4) - C_i(t_3)$
		- i.e. evenly spaced

## Example
Correct a 32768 Hz clock is 100ms ahead:
- sudden subtraction of 100ms
	- satisfies property 1
	- violate property 3 and 4
- therefore a gradual correction is preferred
	- for example, we can treat 32768 + 1 ticks as 1s
		- this would take about $0.1 \times 32768$ seconds to correct
		- See PS 7 Q1

## Synchronization Interval
How often do you synchronize clock $i$?
- assume $C_i$ synchronized at $t_0$
	- $|C_i(t_0) - t_0| < \delta$
- after drifting
	- $|C_i(t)-t| < \delta + \rho(t-t_0)$
	- where $\rho(t-t_0)$ is when to synchronize next
	- $\rho$ isn't a function it's drift rate

See PS7 Q2 :/

## Precision Time Protocol (PTP)
It's a centralized clock synchronization scheme.

- it's more precise (~15-30 ns) compared to its decentralized alternative Network Time Protocol (NTP) (~10 ms)
- it's used in scientific experiments or financial trading (High Frequency Trading)
- it works on a Local Area Network (LAN) rather than the internet (due to bandwidth, latency, and credibility probably)
- it uses a master-slave hierrarchy
	- the time server is a "grandmaster"
		- it uses GPS or cellular network for time reference
		- it has a high-precision oscillator (think cesium-133)
		- broadcast synchronization packets on its network at up to 10 Hz
		- it uses UDP (user datagram protocol) to deliver those packets
- the scheme is an IEEE standard (1588 - 2008)

### Synchronization Sequence

![](attachments/Pasted%20image%2020211207230205.png)

The slave knows $T_1(m), T_1'(s), T_2(s), T_2'(m)$
$$ T_1' = T_1 + \delta + l$$ where $l$ is the network latency
$$ T_2' = T_2 -\delta + l$$
$$\therefore\ 2\delta = T_1' - T_1 + T_2 - T_2'$$


## CNV (Convergence) Algorithm
Example of a distributed clock synchronization algorithm (developed by Leslie Lamport)

![](attachments/Pasted%20image%2020211208000600.png)

- faulty clock can interfere (duh lol)
	- So averaging doesn't always work

At time $t$ all clocks report their time $C_j$, then each clock $C_k$ records the time from each clock as:
$$\tilde{C_j} = \begin{cases}
C_j & \text{if}~~ |C_j-C_k| < \epsilon\\
C_k & \text{otherwise}
\end{cases}$$

Then $C_k$ is evaluated to be the average
$$C_k = \frac{1}{n} \sum_{j=1}^{n} \tilde{C_j}$$

Stopping at 9

# Oral Message (OM) Algorithm