# Semaphores
Semaphores are enumerables with 3 functions:
1. init - initializes the counter value (denote as `s`)
2. wait - decrements `s` by 1, block if `s==0`
3. signal - increments `s`

## Use Cases
### Event Signal
One task signaling to another that an event occured so that the latter can be unblocked.

Common resources:
```
semaphore event = 0; //initialization
```

Task 1:
```
loop forever:
	...
	wait event
	...
```

Task 2:
```
loop forever:
	...
	detect/generate event //blocking
	signal event
```

### Mutual Exclusion
Note that this is not exactly a `mutex` because the semaphore can be "locked"/"unlocked" by any task, not just the owner.

Common resources:
```
//initialization so that it can be acquired by any task right away 
semaphore lock = 1
```

Task:
```
loop forever:
	wait lock //"acquires" the lock
	modify shared resources
	signal lock //"releases" the lock for the next task
```

### Task Rendezvous
Kind of like `thread.join()` for two tasks. Takes two inidvidual semaphores where each task wait for the other's semaphore.

### Barrier Synchronization
Generalizes the two-task rendezvous.

Common resources:
```
n = number of workers;

semaphore turnstile1 = 0;
semaphore turnstile2 = 0;

semaphore sem = 1;
counter = 0;
```

Tasks:
```
loop forever:
	// do work
	
	wait sem
		counter ++;
		if counter == n:
			wait turnstile2
			signal turnstile1
	signal sem
	
	wait turnstile1
	signal turnstile1
	
	wait sem
		counter --;
		if counter == 0:
			wait turnstile1
			signal turnstile2
	signal sem
	
	wait turnstile2
	signal turnstile2
```

# Problems with Synchronization

## Piority Inversion
A high priority task is blocked indirectly by a low priority task
Requires 3(+) tasks and a mutex

### Example: Mars Pathfinder Reset Problem
The communication task had low priority, but it indirectly blocked the higher priority tasks because it acquired a mutex but couldn't release it in time.

The solution is priority inheritance. The low priority task that acquires the same mutex a higher priorty task is blocked on will be promoted to that higher priority for as long as it holds on to the mutex.

## Coffman's 4 Conditions for Deadlock
1. mutual exclusion - when multiple threads need to write to the same resource
2. a task "holds" 1+ resources while it waits for others
3. a cycle exists in the resource allocation graph
4. no pre-emption of resources - a resource can only be released by its owner without OS intervention

Deadlock can be avoided if 1+ of the above conditions is eliminated

## Readers-Writers Problem
For a shared resource that requires multiple readers and writers, satisfy the following criteria:
1. concurrent readers
2. exclusive writers
3. no starvation

### Example pseudo-code
The following implementation satisfies all three conditions: especially the last.

Common resources:
```
semaphore roomEmpty = 1
semaphore turnstile = 1

mutex reader_count = 0
int readers = 0
```

_writer_
```
acquire/wait turnstile

wait roomEmpty
	release/signal turnstile

	write...
signal roomEmpty
```

_reader_
```
acquire/wait turnstile
release/signal turnstile

acquire reader_count
	readers ++
	if (readers == 1)
		wait roomEmpty
release reader_count

read...

acquire reader_count
	readers --
	if (readers == 0)
		signal roomEmpty
release reader_count
```

> Note: semaphore is used for `roomEmpty` because unlike a mutex, it can be "released" by any thread, not just the owner.

> Note: the turnstile is added so that whenever a writer is released into the ready queue, it will have a way to tell the readers "following" it in that queue to stop acquiring the `roomEmpty` semaphore. This way writers are guaranteed to not be starved by a continuous stream of readers