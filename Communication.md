# Producer - Consumer Problem
It's a common paradigm for the "producer" tasks to transmit data to "consumer" tasks. Here are some buffering techniques:

## Handshake Protocol - One Message Buffer
Common resources (global)
```c
const uint32_t buffsize = 512;
uint8_t buf[buffsize];

semaphore empty = 1, full = 0;
```

Producer
```c
while (true) {
	// acquire data (maybe in parallel)
	wait(empty);
	// write data to buffer
	signal(full);
}
```

Consumer
```c
while (true) {
	wait(full);
	// read the whole buffer
	signal(empty);
	// consume data (maybe in parallel)
}
```

## Double Buffering
Uses two buffers (duh lol) and avoids tearing issues.

|producer|consumer|b0_empty|b0_full|b1_empty|b1_full|
|--------|--------|:------:|:-----:|:------:|:-----:|
|        |        |1       |0      |1       |0      |
|wait `b0_empty`  |wait `b0_full`   |0|0|1|0|
|produce         |                |0|0|1|0|
|signal `b0_full` |                |0|1|1|0|
|wait`b1_empty`   |consume         |0|0|0|0|
|produce         |signal `b0_empty`|1|0|0|0|
|signal `b1_full` |wait `b1_full`   |1|0|0|1|

So on and so forth. Notice how the producer and the consumer use buffers in the same order.

Common resources
```c
const uint32_t buffsize = 512;
uint8_t buf[2][buffsize];

semaphore empty=2, full=0;
```

Producer
```c
uint8_t index = 0;
while (true) {
	wait empty;
	// write to buf[index];
	signal full;
	index = (index+1)%2;
}
```

Consumer
```c
uint8_t index = 0;
while (true) {
	wait full;
	// read from buf[index];
	signal empty;
	index = (index+1)%2;
}
```

## Bounded Buffers
`n` buffers for multiple producers and consumers.

Common resources
```c
const uint8_t n = 8;
const uint32_t buffsize = 512;
uint8_t buf[n][buffsize];

uint8_t tail=0, head=0;
mutex tail_mutex=1, head_mutex=1;
```

Producer

> Ahh see problem set 6

# Queueing Theory
Clients arrive and wait in queue to be served.

Data producers in previous problems can be modeled as clients since they "fill in requests"; data consumers can be modeled as servers since they "take requests" and process them.

$$average\ arrival\ rate \equiv \lambda$$
$$average\ service\ rate \equiv \mu$$
$$load\ factor\ \rho = \frac{\lambda}{\mu}$$

The queue will jam/overflow eventually for $\rho>1$.

## Deterministic Process
Where the clients and servers literally send/process messages at the average rate.

## Markov Process
A Markov process is:
- Stochastic -- a sequence of random values
- memoryless -- statistically independent

Kendall notation for queueing models:
$$A/S/c/K$$
<center>Arrival model (D or M) / service model (D or M) / # of servers (1) / queue length (default=infinity)</center>

### M/M/1/$\infty$
Average number of clients in the system (number in the queue + 1 being served)
$$L = \frac{\rho}{1-\rho}$$

Average time a client spends in the system (time in queue + serve time)
$$W = \frac{1/\mu}{1-\rho} = \frac{1}{\mu-\lambda}$$

Little's Law
$$L = \lambda W ~~~~~~~~~~~~~~~ \because \frac{\lambda/\mu}{1-\rho} = \frac{\rho}{1-\rho}$$

Probability of 0 clients in the system
$$P_0 = 1 - \frac{\lambda}{\mu} = 1 - \rho$$

Probability of having exactly $n$ clients in the system
![](attachments/Pasted%20image%2020211122202306.png)
$$\lambda P_n = \mu P_{n+1}$$
$$P_{n+1} = \rho P_n$$
$$P_{n} = \rho^n P_0$$

### M/M/1/K
Probability of 0 clients in the system
$$P_0 = \frac{1-\rho}{1-\rho^{K+1}}$$

Probability of $n$ clients in the system
$$P_n = \rho^n\frac{1-\rho}{1-\rho^{K+1}}$$

Blocking probability
$$P_{block} = P_K$$