# Dynamic Allocation
Acquisition of memory at runtime. It's convenient but non-deterministic. It can fail too.

## glibc malloc
Heap:
- starts empty
- makes system calls (`sbrk()` in Linux) to increase heap size (moves the "program break")
- When `malloc()` calls `sbrk()`, it returns chunks of memory of size 64+ KiB since making system calls are expensive

## Free List
When new "chuncks" of memory gets returned from `sbrk()` or when they are freed, they are added to the free list.

Chunks are stored in "bins", which is an array of linked lists:

![](attachments/Pasted%20image%2020211115224621.png)

![](attachments/Pasted%20image%2020211115224815.png)

If a right-sized chunk isn't found in the free list, `malloc()` will split off a chunk from the unused space

The hidden first bin is a bit vector that represents non-empty bins (to skip over the empty ones during runtime?)

Two types of chunks:

![](attachments/Screenshot%20from%202021-11-15%2022-57-42.png)

Bins as they are stored in memory

![](attachments/Pasted%20image%2020211115230137.png)

When a chunk is freed, it is coalesced with nearby contiguous freed chunks to reduce fragmentation.

## Pointer Mistakes >:(
- losing pointers to allocated chunks
	- causes memory leak
- use a freed block
	- permission issues aside, it will corrupt the free list lists and cause all sorts of havoks x_x
- free an invalid pointer
	- since fields in valid chunks are hard-coded, pointers that point to invalid chunks will confuse `free()` with bad information (about size, etc)
- double free a pointer
	- similar to freeing an invalid pointer because the structure of the chunk isn't a "valid chunk" anymore
- overflow or underflow a block
	- writing to bytes over/below the chunk limit, which corrupts its neighbours

> Valgrind - memory misuse checker

# Static Allocation

Allocation at compile time
- deterministic
- won't fail at runtime
- required for safety-critical systems like avionics, automotive, etc

## RTOS 2
Control blocks can be statically allocated
- 4-byte aligned

Thread stacks can be statically allocated
- 8-byte aligned

### Example
```c
#include <cmsis_os2.h> // RTOS2 API
#include <rtx_os.h> // implementation-specific macros, etc for RTX 5

#define STACK_SIZE 2048 // 2 KiB or 1<<11

uint32_t mutex_cb[osRtxMutexCbSize/4]; // 4-byte aligned
uint32_t thread_cb[osRtxThreadCbSize/4];
uint64_t stack[STACK_SIZE/8]; // 8 B aligned

osMutexId_t mutex;

void worker(void* args) {
	osMutexAcquire(mutex, osWaitForever);
	printf("hello :D\n");
	osMutexRelease(mutex);
}

int main(void) {
	osKernelInitialize();
	
	osMutexAttr_t mAttr = {
		// use the properly aligned control block we just allocated
		.cb_mem = mutex_cb; // the array is the pointer
		
		// that same constant from earlier ^
		.cb_size = osRtxMutexCbSize;
	}
	
	mutex = osMutexNew(&mAttr);
	
	osThreadAttr_t tAttr = {
		// just like the mutex params
		.cb_mem = thread_cb;
		.cb_size = osRtxThreadCbSize;
	}
	osThreadNew(worker, NULL, &rAttr);
	
	osKernelStart();
	while(1);
}
```