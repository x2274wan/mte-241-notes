# Layers
These protocol layers provide structure and modularity

## Protocol Stacks

|Internet Protocol Stack|ISO OSI (Open System Interconnect)|
| :-: | :-: |
| | Application |
| | Presentation |
| Application | Session Layer |
| Transport | Transport |
| Network | Network |
| Link | Link |
| Physical | Physical |

- The OSI Presentation layer provides encryption and compressions, similar to the Application layer in the internet protocol stack
- The OSI Session layer provides checkpointing & sequencing functions, which are sort of covered by the Application and Transport layers in the internet protocol stack


## The Internet Protocol Stack
1. Physical
	- electromagnetic media for signal propagation
	- twisted pair wires, fiber optics, radio
2. Link
	- packages physical signals into "frames"
	- ethernet (ISO 802.3), WiFi (ISO 802.11)
3. Network
	- addressing and provides structure for packet delivery
	- Internet Protocol (IP) v4 & v6
4. Transport
	- specifies how connections are established
	- Transmission Control Protocol (TCP)
	- User Datagram Protocol (UDP)
5. Application
	- as the name implies, how the specific applications make use of packets/information
	- HyperText Transfer Protocol (HTTP), Simple Mail Transfer Protocol (SMTP), etc

When an application on Host A wants to talk to another on Host B, those application layer messages get packaged by a transport layer protocol, which is then sent over the network protocol, where the packets are redirected to other hosts using link -> physical interfaces based on the network protocol (via routers or switches should tree-like topologies apply).

Each layer adds a header to the payload, and are stripped away as they are received.

Some layers (like transport and link) break down (subdivide) data from upper layers to pass to the lower layer, then recombines then upon reception.

![](attachments/Pasted%20image%2020211210235657.png)

# Internet Protocol (IP)
- routes datagrams hop-by-hop across networks
- network routers use forwarding tables to determine which outgoing links are used for which datagrams
- the IP header includes either
	- a 32-bit address (IPv4)
	- a 128-bit address (IPv6)
- Domain Name Servers (DNS) hosts translate domain names to IP addresses

![](attachments/Pasted%20image%2020211211001114.png)

IP is said to be unreliable, meaning that datagrams can be lost, duplicated, arrive out of order, and/or corrupted. Hence the need for different higher-level transport protocols for different levels of robustness depending on the nature of the applications.

# Transmission Control Protocol (TCP)
- it's an end-to-end protocol
	- whereas protocols like IP only concerns with machine-to-machine
- creates a "virtual circuit" between hosts
	- "virtual circuit" more like a managed session with connect and disconnect commands
- full duplex
- ACK (acknowledgement) and segment numbers are exchanged
	- ensures **all data** is received **in order**
- uses checksums
	- ensures **data integrity**
	- ie detects corruption but not necessarily correct them
- data at the top level appears to be a continuous stream of bytes devered in segments
	- maximum segment size depends on the OS on both hosts
- the ends of the "circuit" are called **sockets**
	- sockets are bound with IP addresses and ports
	- applications register ports with the OS
	- ports 0-1023 are reserved for well-known applications
		- 20 - File Transfer Protocol (FTP)
		- 22 - Secure SHell (SSH)
		- 80 - HTTP
		- 143 - Internet Message Access Protocol (IMAP)
		- 443 - HTTP Secure (HTTPS)
		- 587 - SMTP
		- port number is a 16 bit value
			- goes up to 65535

## TCP Header
![](attachments/Pasted%20image%2020211211004024.png)

# User Datagram Protocol (UDP)
- it's **connectionless**
- trades reliability for speed and latency
	- low overhead
	- small header, no ACKs, no sequence numbers
- simplex (one way)

![](attachments/Pasted%20image%2020211211122941.png)

# Socket Programming
Trivia:
- `AF_INET` is IPv4
	- `localhost` dereferences to `127.0.0.1`
- `AF_INET6` is IPv6
	- `localhost` dereferences to `::1`
- `UNIX` is for communication between local processes

## Over TCP
![](attachments/Pasted%20image%2020211211150307.png)

TCP Connection: 3-way handshake
```plantuml-svg
client -> server: SYN
server -> client: SYN+ACK
client -> server: ACK
```
Where `SYN` is the packet containing sequence length negotiation, options, etc

Example server code
```c
#include <arpa/inet.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

int main(void) {
	// open socket - SOCK_STREAM is for TCP
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("error opening socket");
		return 1;
	}
	
	// bind socket
	// htons - host (port) to network
	struct sockaddr_in server_addr = {AF_INET, htons(1101), 0};
	// pton - pretty to numeric - converts string to bytes
	// when the address is known
	inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr);
	if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
		printf("error binding to port\n");
		return 1;
	}
	
	// listen and accept connections
	// second param of listen() is the size of the backlog
	listen(sockfd, 1);
	struct sockaddr_in client_addr;
	socklen_t client_len = sizeof(client_addr);
	// a new socket is created for each client because each socket
	// contains both host and destination IP and port
	int new_sockfd = accept(sockfd, (struct sockaddr*)&client_addr, &client_len);
	if (new_sockfd < 0) {
		printf("error accepting connection");
		return 1;
	}
	
	// receive and send msgs
	uint8_t request[256];
	// n is the return status
	int n = read(new_sockfd, request, sizeof(request));
	if (n<0) {printf("error reading msg"); return 1;}
	printf("server received: %s\n", request);
	char response[] = "response";
	n = write(new_sockfd, response, sizeof(response));
	if (n<0) {printf("error sending msg"); return 1;}
	
	// close connection
	close(new_sockfd);
	close(sockfd);
}
```

Example client code
```c
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

int main(void) {
	// open socket
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd<0) {printf("failed to open socket"); return 1;}
	
	// request connection
	struct hostent* he = gethostbyname("localhost");
	if (!he) {printf("hostname not found"); return 1;}
	struct sockaddr_in server_addr = {AF_INET, htons(1101), 0};
	// memory manipulation to copy the first dereferenced address
	// to the sin_addr member of server_addr
	memcpy(&server_addr.sin_addr, he->h_addr_list[0], he->h_length);
	if (connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
		printf("failed to request connection");
		return 1;
	}
	
	// send and receive msgs
	char request[] = "request";
	int n = write(sockfd, request, sizeof(request));
	if (n<0) {printf("failed to send msg"); return 1;}
	uint8_t response[256];
	n = read(sockfd, response, sizeof(response));
	if (n<0) {printf("failed to receive msg"); return 1;}
	printf("received response: %s\n", response);
	
	// close socket
	close(sockfd);
}
```

## Over UDP
![](attachments/Pasted%20image%2020211211151829.png)
